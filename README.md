# Photobook Kindermuseum

## Preparing Raspberry Pi

### Install Kivy on Raspberry Pi
    
    sudo apt-get update
    sudo apt-get install libsdl2-dev libsdl2-image-dev libsdl2-mixer-dev libsdl2-ttf-dev \
    pkg-config libgl1-mesa-dev libgles2-mesa-dev \
    python-setuptools libgstreamer1.0-dev git-core \
    gstreamer1.0-plugins-{bad,base,good,ugly} \
    gstreamer1.0-{omx,alsa} python-dev libmtdev-dev \
    xclip
    
    sudo pip install -U Cython==0.25.2
    
    sudo pip install kivy

### Install dependencies
    
    sudo pip install -r requirements.txt

### Modify something.
    
- Open a config file with `sudo nano /boot/config.txt` and add `gpu_mem=384` at the end of the file.
- Reboot RPi.
