"""
    Utility to read configurations from config.xml
"""
import sys
import xmltodict


config_file = 'data/config.xml'


def get_xml_configuration():
    if sys.version_info[0] == 2:
        data = xmltodict.parse(open(config_file, 'r'))
    else:
        data = xmltodict.parse(open(config_file, 'rb'))
    return data
