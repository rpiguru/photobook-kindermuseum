import os
from kivy.lang import Builder
from kivy.uix.image import Image

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'image_button.kv'))


class ImageButton(Image):

    def __init__(self, **kwargs):
        super(ImageButton, self).__init__(**kwargs)
        self.register_event_type('on_released')

    def on_touch_down(self, touch):
        super(ImageButton, self).on_touch_down(touch)
        if self.collide_point(*touch.pos) and not self.disabled:
            self.dispatch('on_released')

    def on_released(self, *args):
        pass
